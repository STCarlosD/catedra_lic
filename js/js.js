var info = [
    "<h5>Ahuachapán</h5> Cabecera departamental: Ciudad de Ahuachapán.<br>Extensión terrritorial: 244.8 km²<br>Las fiestas patronales se realizan oficialmente del 22 de febrero al 12 de marzo; siendo el propio día<br> el 27 de febrero que es cuando es celebrada la misa y la imagen recorre las principales calles de la ciudad.<br>Número de habitantes: 129,750 (2017).",
    "<h5>Cabañas</h5>Cabecera departamental:  Sensuntepeque<br>Extensión terrritorial: 1103.51 km²<br>Esta región tiene muchas reservas naturales. El departamento fue nombrado en honor del político centroamericanista hondureño, José Trinidad Cabañas.<br>Las fiestas Patronales de sensuntepeque se realizan del 25 de noviembre al 5 de diciembre de cada año<br>numero de habitantes: 149,326 hab.",
    "<h5>Cuscatlan</h5>Cabecera departamental:Cojutepeque<br>Extensión territorial:756 km²<br>Su economía se fundamenta básicamente en la agricultura que se especifica en el cultivo de la caña de azúcar. Así mismo,  en menor proporción el tabaco, que es de excelente calidad. La manufactura de artesanías es otro de los pilares fundamentales de la economía de este departamento.<br>Celebran sus fiestas patronales del 15 al 29 de enero<br>numero de habitantes: 216 446 hab.",
    "<h5>La Libertad</h5>Cabecera departamental: Ciudad de Santa Tecla<br>Extensión terrritorial: 1653 km²<br>De acuerdo con la estadística del departamento de La Libertad hecha por el gobernador José López en el 23 de mayo de 1865, el departamento tenía una población de 16 759 personas.",
    "<h5>La paz</h5>Cabecera departamental:Zacatecoluca<br>Extensión terrritorial:1,224 km²<br>Destacan en el departamento los ríos que forman parte de las tres cuencas más importantes del país: los ríos Pupuluya-Comalapa, río Jiboa y los ríos Jalponga-El Lempa<br>Celebra sus fiestas patronales del 12 al 24 de Octubre en honor al patrón San Rafael Arcángel.<br>numero de habitantes: 320,379 hab.",
    "<h5>Sonsonate</h5>Cabecera departamental: La ciudad de Sonsonate<br>Extension territorial:232.5 km²<br>En el marco de las actividades económicas primarias pueden considerarse igualmente la pesca fluvial, que tiene un carácter local y de subsistencia, y la comercial en aguas del Pacífico.<br>Oficialmente, los festejos comienzan el sábado 22 de enero y finalizan el 2 de febrero<br>numero de habitantes: 72,951hab",
    "<h5>San Salvador</h5>Cabecera departamental: San Salvador.<br>Extensión terrritorial: 72.25 km²<br>Elevación: 658 msnm <br>Ubicación geográfica: Limita al norte con los municipios de Nejapa, Cuscatancingo y Ciudad Delgado; al este con Soyapango y San Marcos; al sur con Panchimalco y San Marcos y al oeste con Antiguo Cuscatlán y Santa Tecla.",
    "<h5>La Unión</h5>Cabecera departamental: La ciudad y puerto de La Unión.<br>Extensión terrritorial: 2,074 km²<br>Las fiestas patronales en honor a la Virgen del Rosario. Incluso tambíen dentro del departamento de La Unión se celebra el día de la Inmaculada Concepción de María en día 8 de diciembre<br>Número de habitantes: 238,217 hab.",
    "<h5>Morazán</h5>Cabecera departamental: San Francisco Gotera.<br>Extensión terrritorial: 1,447 km²<br>Fiestas de San Francisco Gotera: del 1 al 4 de octubre, en honor de San Francisco de Asís. También se celebra las fiestas en Cacaopera: del 13 al 15 de agosto, en honor de la Virgen del Tránsito<br>Número de habitantes: 252,500 hab.",
    "<h5>San Miguel</h5>Cabecera departamental: San Miguel de la Frontera.<br>Extensión terrritorial: 594 km²<br>Las fiestas son el 21 de noviembre, día de Nuestra Señora de la Paz y luego existe la gran fiesta del Carnaval de San Miguel, siempre se lleva a cabo el ultimo sábado de Noviembre.<br>Número de habitantes: 247,126 hab (2016).",
    "<h5>San Vicente</h5>Cabecera departamental:  San Vicente<br>Extensión terrritorial: 267.2 km²<br>Debido a su producción cafetalera, es uno de los municipios que da grandes aportes a la economía del país; por otro lado, tiene protagonismo a nivel turístico, gracias al volcán San Vicente que atrae mucho la atención de visitantes variados<br>Las fiestas patronales se celebran en el mes de diciembre en honor de san Vicente Abad y Mártir. También son días festivos el 15 de enero en honor al Señor de Esquipulas, y del 25 de octubre al 3 de noviembre, por la \"Feria de todos los Santos\".<br>Número de habitantes: 155 265 hab.",
    "<h5>Usulutan</h5>cabecera departamental: Usulutan<br>Extensión territorial: 139.8 km²<br>Aunque Usulután es un destino de transito hacia otros destinos turísticos cercanos, podrás disfrutar de los sitios como la Casa de la Cultura, volcán de Usulután, laguna del Palo Galán y el muelle de Puerto Parada.<br>La celebración principal es el 25 de noviembre, día de la patrona<br>numero de habitantes: 73,064 hab.",
    "<h5>Chalatenango</h5>Cabecera departamental:  Chalatenango<br>Extensión terrritorial: 2017 km²<br>Se encuentra ubicado al norte de la capital del país, San Salvador, fronterizo con Honduras. En este departamento se ubica el cerro El Pital con 2730 m de altitud sobre el nivel del mar, el punto más elevado del país. El clima más frío de El Salvador tiene lugar en estas alturas.",
    "<h5>Santa Ana</h5>Cabecera departamental: Ciudad de Santa Ana.<br>Extensión terrritorial: 2023 km²<br>Fiestas Julias son denominados los festejos patronales, celebrados del 17 al 26 de julio,​ en la ciudad de Santa Ana en El Salvador.<br>Número de habitantes: 631 100 hab.",
]
window.onload = function () {

    const areas = document.getElementsByClassName("hover-area");
    const hover = document.getElementsByClassName("hover-text");


    for (let i = 0; i < areas.length; i++) {
        areas[i].addEventListener('mouseover', () => {
            hover[i].style.display = 'block';
        }, false);

        areas[i].addEventListener('mouseleave', () => {
            hover[i].style.display = 'none';
        }, false);

    }

}
var tooltip = $('#tooltip');
var map = $('#esa-map');
var state = $('#esa-map g');
var stateNumber = 0;

state.each(function () {

    $(this).on('mouseenter', function () {

        var name = $(this).attr('id');


        tooltip.html(`${info[name]}`).show();
        stateNumber = name;

    }).on('mouseleave', function () {
        tooltip.css({ 'left': 0, 'top': 0 }).hide();
    });



});

tooltip.hide();

$(document).on('mousemove', function (e) {
    if (stateNumber == 7 || stateNumber == 8 || stateNumber == 9 || stateNumber == 11) {
        tooltip.css({
            left: e.pageX - 300,
            top: e.pageY +5
        });
    } else {
        tooltip.css({
            left: e.pageX + 5,
            top: e.pageY + 5
        });
    }
});